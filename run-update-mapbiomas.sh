#!/bin/sh

# Ugly workaround to run the update.sh script in a more cross-platform way
# the best way would be to make a custom image and install those in it's Dockerfile
# but after some thought I concluded that that would be overkill
docker run --rm -i -v "$PWD:/dir" -w /dir "bash:5.0.17@sha256:9c7cfba756e675caead05652033606294f8922ae7740485831463185382cf9fe" bash -c "apk add --no-cache git bzip2 curl gawk; bash update.sh"
